// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'prstame',
    appId: '1:739787334255:web:debae3e840f96b134ba39a',
    storageBucket: 'prstame.appspot.com',
    apiKey: 'AIzaSyDTqW7JY1oY_JL24Ym-q7cfE5f8yYVFZq8',
    authDomain: 'prstame.firebaseapp.com',
    messagingSenderId: '739787334255',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
